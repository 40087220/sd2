﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercises
{
    class Bus : Vehicle
    {
        public Bus()
        {
            Console.WriteLine("Vehicle, Bus: ");
        }

        private int seats;
        public int Seats
        {
            get { return seats; }
            set { seats = value; }
        }

        public void Display()
        {
            Console.WriteLine("Vehicle Reg No: " + RegNo +
                " " + "Seats: " + Seats);
        }

    }
}
