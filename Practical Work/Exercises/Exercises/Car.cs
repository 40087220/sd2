﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercises
{
    class Car : Vehicle
    {
        public Car()
        {
            Console.WriteLine("Vehicle, Car: ");
        }

        private int doors;
        public int Doors
        {
            get { return doors; }
            set { doors = value; }
        }

        public void Display()
        {
            Console.WriteLine("Vehicle Reg No: " + RegNo +
                " " + "Doors: " + Doors);
        }
    }
}
