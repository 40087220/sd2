﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercises
{
    class Lorry : Vehicle
    {
        public Lorry()
        {
            Console.WriteLine("Vehicle, Lorry: ");
        }

        private int weight;
        public int Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        public void Display()
        {
            Console.WriteLine("Vehicle Reg No: " + RegNo +
                " " + "Weight: " + Weight);
        }
    }
}
