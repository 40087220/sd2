﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            Lorry lorry1 = new Lorry();
            lorry1.RegNo = "ABCDEF12";
            lorry1.Weight = 123;
            lorry1.Display();

            Bus bus1 = new Bus();
            bus1.RegNo = "ISHUEYW";
            bus1.Seats = 122;
            bus1.Display();

            Car car1 = new Car();
            car1.Doors = 3;
            car1.RegNo = "HSGY WGSHHG";
            car1.Display();


            Console.ReadKey();
        }
    }
}
