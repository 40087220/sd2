﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD2Pract1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Prompts user to input their name.
            Console.WriteLine("Please enter your name: ");

            //Creates a string called name.
            String name = Console.ReadLine();

            Console.WriteLine("Hello " + name);

            Console.WriteLine("Press return to exit.");
            Console.ReadLine();
        }
    }
}
