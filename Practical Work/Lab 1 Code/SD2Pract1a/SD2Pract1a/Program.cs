﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD2Pract1a
{
    class Program
    {
        static void Main(string[] args)
        {
            //Initialise vaariables.
            double rLength, rWidth, lLength, lWidth, kLength, kWidth, hLength, hWidth, rArea, lArea, kArea, hArea, tArea;

            //Prompt the user to enter in values.
            Console.WriteLine("Please enter the Length of room: ");
            rLength = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Please enter the Width of room: ");
            rWidth = Convert.ToDouble(Console.ReadLine());
            rArea = rLength * rWidth;

            Console.WriteLine("Room Area: " +rArea);
            //
            //
            //
            Console.WriteLine("Please enter the Length of lounge: ");
            lLength = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Please enter the Width of lounge: ");
            lWidth = Convert.ToDouble(Console.ReadLine());
            lArea = lLength * lWidth;

            Console.WriteLine("Lounge Area: " +lArea);
            //
            //
            //
            Console.WriteLine("Please enter the Length of kitchen: ");
            kLength = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Please enter the Width of kitchen: ");
            kWidth = Convert.ToDouble(Console.ReadLine());
            kArea = kLength * kWidth;

            Console.WriteLine("Kitchen area: " + kArea);
            //
            //
            //
            Console.WriteLine("Please enter the Length of hall: ");
            hLength = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Please enter the Width of hall: ");
            hWidth = Convert.ToDouble(Console.ReadLine());
            hArea = hLength * hWidth;

            Console.WriteLine("Hall area: " + hArea);

            //Calculate total area of the house.
            tArea = rArea + lArea + kArea + hArea;
            Console.WriteLine("Total house area: " +tArea);







            //Prompt the user to exit the program.
            Console.WriteLine("Press return to exit.");
            Console.ReadLine();

        }

    }
}
