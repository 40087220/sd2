using System;

namespace Classes5
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{

			// Put code here to create 3 car instances
            AnotherCar car1 = new AnotherCar();
            car1.Colour = "Blue";
            car1.Cost = 120000;
            car1.Make = "Nissan";
            car1.Type = "GTR";

            //Console.WriteLine(
            //    "Car 1 is a:" +
            //    " " + car1.Make + 
            //    " " + car1.Type +
            //    " " + car1.Colour +
            //    " " + car1.Country + 
            //    " " + car1.Cost);

            car1.Display();

            AnotherCar car2 = new AnotherCar("Nissan");
            car2.Colour = "Black";
            car2.Cost = 100000;
            car2.Type = "Skyline";

            //Console.WriteLine(
            //    "Car 2 is a:" +
            //    " " + car2.Make +
            //    " " + car2.Type +
            //    " " + car2.Colour +
            //    " " + car2.Country +
            //    " " + car2.Cost);

            car2.Display();

            AnotherCar car3 = new AnotherCar("Nissan", "Cubes");
            car3.Colour = "Greeeeen";
            car3.Cost = 100000;
            car3.Type = "Skyline";

            //Console.WriteLine(
            //    "Car 3 is a:" +
            //    " " + car3.Make +
            //    " " + car3.Type +
            //    " " + car3.Colour +
            //    " " + car3.Country +
            //    " " + car3.Cost);

            car3.Display();


			// Add a method named 'Display'to the AnotherCar Class
			// This method should output the details of a  car instance
			// to the Console

			// Put code here to call that method to display the 
			// details of a each car instance 

            Console.ReadKey();
		}
	}
}
